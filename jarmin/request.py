from http.server import BaseHTTPRequestHandler
from pathlib import Path

import response


class RequestHandler(BaseHTTPRequestHandler):
    """
    A simple HTTP server which only responds to HTTP GET and HEAD
    methods.
    It subclasses `http.server.BaseHTTPRequestHandler`.
    The POST method may be implemented later.
    """

    def __init__(self, *args, **kwargs):
        super(RequestHandler, self).__init__(*args, **kwargs)
        self.headers = {}

    def do_GET(self) -> None:
        success = False
        suffix = Path(self.path).suffix
        try:
            if suffix in response.HTMLHandler.accepted_files:
                handler = response.HTMLHandler()
            elif suffix in response.JSONHandler.accepted_files:
                handler = response.JSONHandler()
            elif suffix in response.StaticHandler.accepted_files:
                handler = response.StaticHandler()
            else:
                raise ValueError
            handler.load_content(self.path)

        except (FileNotFoundError, KeyError):
            handler = response.BadRequestHandler(404)
            handler.load_content()
        except PermissionError:
            handler = response.BadRequestHandler(403)
            handler.load_content()
        except UnicodeError:
            handler = response.BadRequestHandler(500)
            handler.load_content()
        except ValueError:
            handler = response.BadRequestHandler(400)
            handler.load_content()

        else:
            success = True

        finally:
            if success:
                self.respond(handler)
                return
            self.send_error(handler.status_code, handler.content.decode())

    def do_HEAD(self) -> None:
        self.send_response(self.headers["status_code"])
        self.send_header("Content-type", self.headers["content_type"])
        self.end_headers()

    def do_POST(self) -> None:
        self.send_response(501)
        self.end_headers()

    def handle_http(self, handler: response.Handler) -> str:
        self.headers["status_code"] = handler.status_code
        self.headers["content_type"] = handler.content_type
        self.do_HEAD()
        return handler.content

    def respond(self, handler: response.Handler) -> None:
        content = self.handle_http(handler)
        self.wfile.write(content)
