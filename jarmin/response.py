"""The Different handlers for HTTP GET requests."""
from abc import ABC, abstractmethod
from json import dumps
from mimetypes import guess_type, guess_all_extensions
from pathlib import Path


class Handler(ABC):
    """
    This is just a base class for all the concrete handlers.
    This class  cannot (and cannot) be instantiated.
    It exists only to help in type hinting.

    Attributes
    ----------
    content : bytes
        The data contained within whichever file was requested.
    content_type : str
        The content type of the content attribute
    status_code : int
        The HTTP response code to be returned with the request.

    Methods
    -------
    load_content
        Get the requested data from its file.
    """

    accepted_files = ()

    def __init__(self) -> None:
        self.content = b""
        self.content_type = ""
        self.status_code = 0

    @abstractmethod
    def load_content(self, filename: str) -> None:
        """Get the requested data from its file."""


class BadRequestHandler(Handler):
    """Handles requests when there is a client or server error."""

    __slots__ = ("status_code", "content", "content_type")

    def __init__(self, status_code: int) -> None:
        super(BadRequestHandler, self).__init__()
        self.status_code = status_code
        self.content_type = "text/plain"
        self.content = b""

    def load_content(self, filename: str = "") -> None:
        """
        Get the file containing self.content.

        Parameters
        ----------
        filename
            Extra contextual information that can be added to make the
            error message easier to read/debug.
            It is called filename for compatibility with the Handler API.

        Return
        ------
        bool
            Whether or not the operation was successful.
        """

        self.content = {
            400: b"400 Bad Request\n%b",
            403: b"403 Forbidden\n%b",
            404: b"404 Not Found\n%b",
        }.get(self.status_code, b"500 Server Error\n%b")
        self.content %= filename.encode("utf8")


class JSONHandler(Handler):
    """Handles GET requests for JSON files."""

    accepted_files = guess_all_extensions("application/json", strict=False)
    base_dir = Path(__file__).parent.parent / Path("static")
    __slots__ = ("status_code", "content", "content_type")

    def __init__(self) -> None:
        super(JSONHandler, self).__init__()
        self.content = b""
        self.status_code = 200
        self.content_type = "application/json"

    def load_content(self, filename: str) -> None:
        """
        Get the file containing self.content.

        Parameters
        ----------
        filename
            Extra contextual information that can be added to make the
            error message easier to read/debug.

        Return
        ------
        bool
            Whether or not the operation was successful.
        """
        filename = filename[1:] if filename.startswith("/") else filename
        filename = filename if filename.endswith(".json") else filename + ".json"
        file_path = self.base_dir / filename
        self.content = dumps(file_path.read_text()).encode("utf8")


class StaticHandler(Handler):
    """Handles GET requests for static files (such as JS and CSS files)."""

    __slots__ = ("status_code", "content", "content_type")
    accepted_files = (
        ".css",
        ".jpg",
        ".jpeg",
        ".png",
        ".ico",
        ".gif",
        ".mp3",
        ".zip",
        ".ttf",
        ".otf",
        ".woff",
        ".js",
        ".xml",
    )
    base_dir = Path(__file__).parent.parent / "static"

    def __init__(self):
        super(StaticHandler, self).__init__()
        self.content = b""
        self.status_code = 200
        self.content_type = "text/plain"

    def load_content(self, filename: str) -> None:
        """
        Get the file containing self.content.

        Parameters
        ----------
        filename
            The name of the file to get self.content from.
        """

        file_path = self.base_dir / filename.lstrip("/")
        self.content = file_path.read_bytes()
        self.content_type = guess_type(str(file_path))[0]

        if not self.content:
            self.status_code = 204


class HTMLHandler(Handler):
    """Handles GET requests for HTML files."""

    accepted_files = ("", *guess_all_extensions("text/html"))
    base_dir = (Path(__file__).parent.parent / "templates").resolve()
    __slots__ = ("status_code", "content", "content_type")

    def __init__(self) -> None:
        super(HTMLHandler, self).__init__()
        self.content_type = "text/html"
        self.content = bytes()
        self.status_code = 200

    def load_content(self, filename: str) -> None:
        file = self.route(filename)
        if not (file.endswith(".html") or file.endswith(".htm")):
            file = file + ".html"

        if (self.base_dir / file).exists():
            file_path = self.base_dir / Path(file)
        else:
            file_path = self.base_dir / Path(file[:-1])

        self.content = file_path.read_bytes()

    @staticmethod
    def route(url: str) -> str:
        """
        Routes the URL paths given to the HTML files they represent.

        Parameters
        ----------
        uri
            The URL of the request.

        Returns
        -------
        str
            The path to the url's HTML file.
        """

        if (not url) or url == "/":
            return "index.html"
        return Path(url).name
