from argparse import ArgumentParser

PARSER = ArgumentParser(
    prog="jarmin",
    description="Jarmin: a simple, lightweight Python3 static site generator.",
)

PARSER.add_argument(
    "-H",
    "--host",
    action="store",
    default="127.0.0.1",
    help="The host on which to run Jarmin.",
)
PARSER.add_argument(
    "-p",
    "--port",
    action="store",
    default=8080,
    help="The port on which to run Jarmin.",
    type=int,
)
