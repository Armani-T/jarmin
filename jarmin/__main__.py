from http.server import HTTPServer
from sys import stdout
from time import asctime

from request import RequestHandler
from command_line import PARSER

args = PARSER.parse_args()

CONFIG = {"host": args.host, "port": args.port}

HTTPD = HTTPServer((CONFIG["host"], CONFIG["port"]), RequestHandler)
stdout.write(
    "Server Started at %s\nServer Running on %s:%d\n%s"
    % (asctime(), CONFIG["host"], CONFIG["port"], "-" * 42)
)

try:
    HTTPD.serve_forever()
except KeyboardInterrupt:
    HTTPD.server_close()
stdout.write("%s\nServer Closed at %s" % ("-" * 42, asctime()))
