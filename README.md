# Jarmin

A lightweight static site generator designed for pure python.
It should work on any Python version >= 3.4

## Features

- Simple
- Lightweight (only takes up x MB of meory to run)
- Pre-configured to run on localhost
- No clutter (just the source code itself and the tests)

## Notes

I am a single student, so I may be unable to work on the project consistently but I'll try to at least approve pull requests.


# Contacts

Name: **Armani Tallam**
E-Mail: armanitallam@gmail.com
